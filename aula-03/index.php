<?php
echo "<pre>";
// Constante PHP
// Constante em maiusculo

define('QTD_PAGINAS', 10);

echo "Valor da minha constante é: " . QTD_PAGINAS;

// Variavel para passar valor para uma constante

$valor2 = 11;

define('QTD_PAGINAS_2', $valor2);

echo "<br>O segundo valor é: " . QTD_PAGINAS_2;

$ip_do_banco = '192.168.45.12';

define('IP_DO_BANCO', $ip_do_banco);

echo "<br> O IP do banco é : " . IP_DO_BANCO;


echo "Estou na linha: " . __LINE__;
echo "<br> Estou no arquivo: " . __FILE__;

$dias_da_semana = ['dom', 'ter', 'qua', 'qui', 'sex', 'sab', 'seg'];
echo "\n";
var_dump($dias_da_semana);

// unset($dias_da_semana); (Destruir variavel)
// var_dump($dias_da_semana);

echo "<\pre>";



